

export const keyGenerator = () => (
    Math.random().toString(36).substr(2, 10)
);
export const randomId = () => {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
}

export const isObject = (obj) => {
    return (typeof obj === "object" && obj !== null) || typeof obj === "function";
}

export const meterToMailes = (meter) => {
    return parseFloat(meter * 0.000621371192)
}

export const toHHMMSS = (duration) => {
    var sec_num = parseInt(duration, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

export const isNumber = (number) => {
    try {
        return number.replace(/[^0-9\.]/g, '')
    } catch (error) {
        console.log("number", number)
        console.warn("" + error)
    }

}

export const isBoxTruckMove = (job ) => {
    if (job.job_type == "Moving" && (job.job_sub_type == "Box Truck Move" || job.job_sub_type == "Pickup Truck Move")) {
        return true;
    }
    return false;
}

/**
 * capitalize 
 * @param {text} str 
 */
export const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}