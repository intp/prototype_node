import { combineReducers } from 'redux';
import auth from './auth_reducer';



import { REHYDRATE, PURGE, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // or whatever storage you are using
//import { reducer as formreducer} from 'redux-form'




const config = {
  key: 'primary',
  storage
}

//  export default combineReducers({
//     // form:formreducer,
//      auth,
//      jobs,
//      nav,

//  });

export default persistCombineReducers(config, {
  // form:formreducer,
  auth,
 

});

