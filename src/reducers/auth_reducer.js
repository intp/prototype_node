import {

    LOGIN_ERROR,
    REGISTER, ATTEMPT,
    LOGIN_SUCCESS, LOGIN_AUTH_ERROR,
    LOG_OUT,
    REGISTER_WORKERS_ERROR, CLEAR_ERROR,CLEAN_AUTH,
} from '../actions/actionsType';
import config from '../constants/config';
// import { SocketServer, SocketJobs } from '../../actions/SocketServer';

const authInit = {
    loading: false,
    token: null,
    successLogin: false,

    user_data: [],
    user_meta: [],
  
    isRegister: false,
    msg: "",
    isRegisterWorkers: false,
    statusAction: false,
    showResults: false,
    socket_token: null,
    unLockPageMain: false,
    isCheckAuth: false,
    updateInfo: false,
    coordinate: {
        lat: Number(config.coords.lat),
        lng: Number(config.coords.long)
    },
}
export default function (state = {}, action) {
    switch (action.type) {
        case ATTEMPT:
            return {
                ...state,
                loading: true
            };
        case LOGIN_SUCCESS:

            return {
                ...state,
                loading: false,
                isRegister: false,
                token: action.payLoad,
                user_data: action.user_data,
                user_meta: action.user_meta,
            
                showResults: true,
                statusAction: true,
                successLogin: true,
                // socket_token: action.socket_token ? action.socket_token : state.socket_token,
            };
        case LOGIN_AUTH_ERROR:
            return {
                ...state,
                loading: false,
                token: null,
                successLogin: false,
                socket_token: null,
                showResults: true,
                statusAction: false,
            }
        case LOGIN_ERROR:
            return {
                ...state,
                msg: action.msg,
                showError: true,

                loading: false,
                token: null,
                showResults: true,
                statusAction: false,
            };

       
      
        case CLEAR_ERROR:
            return {
                ...state,
                showError: false,
                msg: '',

                isRegisterWorkers: false,
                showResults: false,
                statusResults: false,
            }
        case CLEAN_AUTH:
            return {
                ...state,
                showError: false,
                msg: '',
                isRegister: false,
                isRegisterWorkers: false,
                showResults: false,
                statusResults: false,
                user_data: [],
                user_meta: [],
               
            }
        case LOG_OUT:

            return {
                ...state,
                loading: false,
                token: null,
                user_data: [],
                user_meta: [],
              
                isRegister: false,
                msg: "",
                isRegisterWorkers: false,
                statusAction: false,
                showResults: false,
                socket_token: null,
                successLogin: false,
                isCheckAuth: false,
            }

     
        default:
            return state;
    }
}

