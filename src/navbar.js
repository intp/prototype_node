import React, { useState, useEffect } from 'react';
import { Navbar, Nav, NavDropdown, Form, Image, Button,FormControl } from 'react-bootstrap';

import { connect } from "react-redux";
import * as actions from './actions';
import store from './store';

import './App.css';
import config from './constants/config';


class NavBar extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }
    componentWillMount() {
        console.log(config.n)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.token) {

        }
    }

    logout = async () => {
        // this.props.logOut()
        // style={{ backgroundColor: "#2C3E50", color: "#fff" }}
    };

    render() {
        return (
            <>
            <Navbar bg="dark" expand="lg" variant="dark"

            >
              <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                  <Nav.Link href="#home">Home</Nav.Link>
                  <Nav.Link href="#link">Link</Nav.Link>
                  <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                  </NavDropdown>
                </Nav>
                <Form inline>
                  <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                  <Button variant="outline-success">Search</Button>
                </Form>
              </Navbar.Collapse>
            </Navbar>
            </>
        );
    }
}

function mapStateToProps({ auth }) {

    return {
        token: auth.token,
        user_data: auth.user_data,
        user_meta: auth.user_meta
    };
}
export default connect(mapStateToProps, actions)(NavBar);

