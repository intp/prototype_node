



export const ATTEMPT="ATTEMPT"
export const LOGIN = 'login';
export const LOGIN_ATTEMPT = 'login_attempt';
export const LOGIN_ERROR = 'login_error';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_AUTH_ERROR = 'login_auth_error';
export const LOG_OUT = 'log_out';
export const CLEAN_AUTH="CLEAN_AUTH"
export const CLEAR_ERROR="CLEAR_ERROR"