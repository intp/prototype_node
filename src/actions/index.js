




import store from '../store';


export const unauthorizedRequest = async (url, args, method = "POST") => {
  try {
    let formData = new FormData();

    Object.keys(args).map((key, index) => {
      let val = args[key];
      if (key == "image") {

        formData.append(key, val);
      } else if (_.isArray(val) || _.isObject(val)) {
        formData.append(key, JSON.stringify(args[key]));
      } else {
        formData.append(key, args[key]);

      }

    });

    let response = await fetch(
      url, {
        method: method,
        headers: {
          // 'Content-Type': 'multipart/form-data'
        },
        body: formData
      }
    );
    const text = await response.text(); // Parse it as text
    // console.warn("text",text);
    const responseJson = JSON.parse(text); // Try to parse it as json

    return responseJson;
  } catch (error) {

    throw new Error(error);
  }
}

export const authorizedRequest = async (url, args, method = "POST") => {
  try {
    let storee = store.getState();

    let param = {
      auth_token: storee.auth.token,
      user_id: storee.auth.user_data.ID,
    }
    param = { ...param, ...args };
    let response = await unauthorizedRequest(url, param, method)
    return response
  } catch (error) {
    throw new Error(error);
  }

}