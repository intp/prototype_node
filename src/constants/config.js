

import localConfig from '../localConfig'
const optionsEndPoint={
  version:"v2",
}
const name =  process.env.APP_TITLE;
const urlPro= process.env.END_POINT;

const endpoint = process.env.NODE_ENV === 'production' ? `${urlPro}/wp-json/wp/${optionsEndPoint.version}` : `${localConfig.endpoint}/wp-json/wp/${optionsEndPoint.version}`;
const urlHome = process.env.NODE_ENV === 'production' ? `${urlPro}` : `${localConfig.endpoint}`;
const server_socket = process.env.NODE_ENV === 'production' ? `${urlPro}` : `${localConfig.serverSocket}`;
const plat_form = "website";
export default {
  endpoint,
  urlHome,
  name,
  plat_form,
  logo: "",
  phone: ' (303) 536-5126',
  address: " Denver, CO ",
  socialMedia: {
    Facebook: `https://www.facebook.com/${name}/`,
    twitter: `https://twitter.com/${name}`,
    googlePluse: `https://plus.google.com/${name}`,
    yelp: `http://yelp.com/biz/${name}`,
    instagram: `http://instagram.com/${name}/`
  },
 
  coords: {
    lat: 37.78825,
    long: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421 * 50,
  },
  region: {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.1,
    longitudeDelta: 0.1
  },
  coordinate: {
    lat: 37.78825,
    lng: -122.4324,
  },
  DEFAULT_PADDING: { top: 40, right: 40, bottom: 40, left: 40 },

  fontFamily: "Papyrus",


  placeholderTextColor: "rgba(0, 0, 0, 0.42);",


};